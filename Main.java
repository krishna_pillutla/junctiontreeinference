import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class Main {
	public static void main(String[] args) throws IOException, Exception
	{
		/*String path = "/home/ashwin/Acads/AdvML/JunctionTreeInference/TestCases/";
		int ii = 1;
		String graphfile = path+"graph_" + ii;
		String potfile = path+"potentials_" + ii;
		String queryFilename = path+"query" + ii;*/
		String graphfile = args[0];
		String potfile = args[1];
		String queryFilename = args[2];

		Graph G = Graph.ReadGraph(graphfile,potfile);
		Graph G1 = G.Triangulate();
		//System.out.println();
		//System.out.println(G1.numNodes + " " + G1.numEdges + " " + G1.NumExtraEdges);
		
		System.out.println("Edges added during triangulation: " + G1.NumExtraEdges);
		
		JTreeRooted JT= G1.createJT(G1.MaxCliques());
		
		// Read and answer queries
		ArrayList<int[]> qlist = JTreeRooted.ReadQueryFile(queryFilename);
		//int[] map = JT.MAP();
		//System.out.println("MAP Assignment: " + Arrays.toString(map));
		
		//qlist.remove(0);
		JT.printShit();
		Potential[] p = JT.AnswerQuery(qlist);
		for(int i = 0; i < p.length; i++)
		{
			System.out.println("QUERY " + i);
			System.out.println(p[i].nodes.toString());
			System.out.println(p[i].table.toString());
		}
		
	}
}
