import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mikera.arrayz.NDArray;


public class Potential{
	ArrayList<Integer> nodes;
	ArrayList<Node> clique;
	NDArray table;
	
	public Potential(){
		this.clique = new ArrayList<Node>();
		this.nodes = new ArrayList<Integer>();
		this.table = NDArray.newArray(0);
	}
	public Potential Clone(){
		// Node does not have deep copy
		Integer [] temp = new Integer [nodes.size()];
		nodes.toArray(temp);
		ArrayList <Integer> cli = new ArrayList <Integer> (Arrays.asList(temp.clone()));
		Node [] temp1 = new Node [clique.size()];
		clique.toArray(temp1);
		ArrayList <Node> nod = new ArrayList <Node> (Arrays.asList(temp1.clone()));
		return new Potential (  nod, table.exactClone(),cli);
	}
	public Potential(ArrayList<Node> clique, NDArray table)
	{
		this.clique = clique;
		this.table = table;
	}
	public Potential(ArrayList<Node> clique, NDArray table, ArrayList<Integer> nodes)
	{
		this.clique = clique;
		this.table = table;
		this.nodes = nodes;
	}
	
	public Potential[] ReadPotentials(String filename, Graph G) throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line = " "; 
		ArrayList<Potential> array = new ArrayList<Potential>();
		Potential p;
		NDArray table = NDArray.newArray(0); //arbit, just to initialize
		ArrayList<Node> clique = new ArrayList<Node>();
		ArrayList<Integer> nodes = new ArrayList<Integer>();
		int[] shape;
		boolean init = false;
		String[] parts;
		while ((line = br.readLine()) != null)
		{
			parts = line.split(" ");
			//System.out.println(line);
			if (line.charAt(0) == '#') //create new potential
			{
				if (init)
				{
					p = new Potential(clique, table, nodes);
					array.add(p);
				}
				else
				{
					init = true;
				}
				//clique = new ArrayList<Node>(parts.length - 1);
				//nodes = new ArrayList<Integer>(parts.length - 1);
				Node[] cliqueA = new Node[parts.length - 1];
				Integer[] nodeA = new Integer[parts.length - 1];
				shape = new int[parts.length - 1];
				for (int i = 1; i < parts.length; i++)
				{
					int index = Integer.parseInt(parts[i]);

					//node may not be the same as index					
					Node n = G.nodes.get(index);
					nodeA[i-1] = index; //i starts from 1
					cliqueA[i-1] = n;
					shape[i-1] = n.maxValue;
				}
				clique = new ArrayList<Node>(Arrays.asList(cliqueA));
				nodes = new ArrayList<Integer>(Arrays.asList(nodeA));
				table = NDArray.newArray(shape);
			}
			else  //parse the line to fill the table
			{
				int[] indices = new int[parts.length - 1];
				for (int i = 0; i < indices.length; i++)
				{
					indices[i] = Integer.parseInt(parts[i]);
				}
				double pot_val = Double.parseDouble(parts[parts.length - 1]);
				table.set(indices, pot_val);
			}
		}
		p = new Potential(clique, table, nodes);
		array.add(p);
		br.close();
		return array.toArray(new Potential[array.size()]);
	}
	
}
