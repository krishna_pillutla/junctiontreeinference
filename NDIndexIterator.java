
public class NDIndexIterator {
	int length;
	public int[] Iterator;
	public int[] MaxIterator;
	int index;
	boolean EndReached = false;
	int exclude_index, sub_index; //unused
	public int[] SubIterator;    //unused
	NDIndexIterator(int[] max)
	{
		MaxIterator = max;
		length = max.length;
		Iterator = new int[length];
		index = sub_index = 0;
		EndReached = false;
		exclude_index = 0;
		SubIterator = new int[length - 1];
	}
	NDIndexIterator(int[] max, int excl_index)
	{
		MaxIterator = max;
		length = max.length;
		Iterator = new int[length];
		index = sub_index = 0;
		EndReached = false;
		exclude_index = excl_index;
		SubIterator = new int[length - 1];
	}
	void Refesh()
	{
		Iterator = new int[length];
		index = 0;
		EndReached = false;
	}
	
	boolean increment() //returns true if incremented
	{
		int i= 0;
		while (Iterator[i] == MaxIterator[i])
		{
			i++;
			if (i == length) return false; //max reached
		}
		Iterator[i]++;
		for (int j = 0; j < i; j++)
		{
			Iterator[j] = 0;
		}
		return true;
	}

	/*
	boolean increment() //returns true if incremented
	{
		if (Iterator[index] < MaxIterator[index]) 
		{
			Iterator[index]++;
			if (index != exclude_index) SubIterator[sub_index]++;
		}
		else
		{ //max elem of this row reached
			if (index != exclude_index) sub_index++;
			index++;
			if (index == length) EndReached = true;
			else {
				Iterator[index]++;
				if (index != exclude_index) SubIterator[sub_index]++;
			}
		}
		return !EndReached;
	}
	*/
}
