import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import mikera.arrayz.INDArray;
import mikera.arrayz.NDArray;



public class JTNode {
	String label;
	List<Node> Contents;
	int id;
	Potential potential;
	Potential ghanta; 
	Map<JTNode, JTEdge> EdgeWith;
	public JTNode(List<Node> contents)
	{
		this.Contents = contents;
		EdgeWith = new HashMap<JTNode, JTEdge>();
		potential = new Potential();
		ghanta = potential.Clone();
	}
	public void updatePotential(Potential newPot){
		potential = newPot;
		ghanta = newPot.Clone();
	}
	public void multPotential(Potential newPot){
		if(potential.nodes.size() == 0){
			potential = newPot.Clone();
			ghanta = potential.Clone();
			return;
		}
		ArrayList <Integer> oldNodesList = new ArrayList<Integer> (potential.nodes);
		ArrayList <Integer> newNodesList = new ArrayList<Integer> (newPot.nodes);
		HashSet <Integer> oldNodes = new HashSet<Integer> (potential.nodes);
		HashSet <Integer> newNodes = new HashSet<Integer> (newPot.nodes);
		HashMap <Integer, Integer> oldPotMap = new HashMap<Integer,Integer>();
		HashMap <Integer, Integer> newPotMap = new HashMap<Integer,Integer>();

		HashSet <Integer> temp = new HashSet<Integer> (oldNodes);
		temp.retainAll(newNodes);
		ArrayList<Integer> intersection = new ArrayList<Integer> (temp);
		HashSet <Integer> temp1 = new HashSet<Integer> (oldNodes);
		temp1.removeAll(intersection);
		ArrayList<Integer> oldOnly = new ArrayList<Integer> (temp1);
		HashSet <Integer> temp2 = new HashSet<Integer> (newNodes);
		temp2.removeAll(intersection);
		ArrayList<Integer> newOnly = new ArrayList<Integer> (temp2);
		ArrayList<Integer> union = new ArrayList<Integer>(oldOnly);
		union.addAll(intersection);
		union.addAll(newOnly);
		//System.out.println("New Potential:"+ union.toString());
		int intersectionStartIndex = oldOnly.size();
		int newOnlyStartIndex = oldOnly.size()+intersection.size();
		for(int i=0;i<union.size();i++){
			if(i<intersectionStartIndex){
				oldPotMap.put(i, oldNodesList.indexOf(union.get(i)));
			}
			else if(i<newOnlyStartIndex){
				oldPotMap.put(i, oldNodesList.indexOf(union.get(i)));
				newPotMap.put(i, newNodesList.indexOf(union.get(i)));
			}
			else{
				newPotMap.put(i, newNodesList.indexOf(union.get(i)));
			}
		}
		
		/* Create a new table */
		int[] old_shape =  potential.table.getShape();
		int[] new_shape =  newPot.table.getShape();
		int[] com_shape = new int [union.size()];
		for(int i=0;i<union.size();i++){
			if(i<intersectionStartIndex){
				com_shape[i] = old_shape[oldPotMap.get(i)];
			}
			else if(i<newOnlyStartIndex){
				if(old_shape[oldPotMap.get(i)] != new_shape[newPotMap.get(i)]){
					//System.err.println("Potential Shapes don't match");
				}
				com_shape[i] = old_shape[oldPotMap.get(i)];
			}
			else{
				com_shape[i] = new_shape[newPotMap.get(i)];
			}
		}
		NDArray com_table = NDArray.newArray(com_shape);
		com_table.fill(1);
		NDArray old_table = potential.table;
		NDArray new_table = newPot.table;


		/* Populate with values */
		int[] new_iterator = new int [newNodes.size()];
		int[] old_iterator = new int [oldNodes.size()];
		int[] maxEntry = com_shape.clone();
		for (int i = 0; i < maxEntry.length; i++) maxEntry[i]--; //maxEntry[i] = old_shape[i] - 1;
		//System.err.println(Arrays.toString(new_shape));
		//System.err.println(Arrays.toString(old_shape));
		//System.err.println(Arrays.toString(com_shape));
		NDIndexIterator I = new NDIndexIterator(maxEntry);

		do
		{
			for(int i=0;i<union.size();i++){
				if(i<intersectionStartIndex){
					old_iterator[oldPotMap.get(i)]= I.Iterator[i];
				}
				else if(i<newOnlyStartIndex){
					old_iterator[oldPotMap.get(i)]= I.Iterator[i];
					new_iterator[newPotMap.get(i)]= I.Iterator[i];
				}
				else{
					new_iterator[newPotMap.get(i)]= I.Iterator[i];
				}
			}
			//can be either addition or maximum
			com_table.set(I.Iterator, old_table.get(old_iterator)*new_table.get(new_iterator) ); //increment by the current iterator's value
		} while (I.increment());
		potential.nodes = union;
		potential.table = com_table;
		ghanta = potential.Clone();
	}
	public void dummyFill(){
		if(potential.nodes.size()==0){
			potential.table = NDArray.newArray(Contents.get(0).maxValue);
			potential.table.fill(1);
			potential.nodes.add(Contents.get(0).name);
		}
		for(Node n : Contents){
			if(!potential.nodes.contains(n.name)){
				int [] old_shape= potential.table.getShapeClone();
				int [] new_shape= new int[old_shape.length+1];
				for(int i=0;i<old_shape.length;i++){
					new_shape[i] = old_shape[i];
				}
				//System.out.println("max = "+n.maxValue);

				new_shape[new_shape.length-1] = n.maxValue;
				//System.out.println(Arrays.toString(potential.table.getShape()));
				//System.out.println(Arrays.toString(new_shape));
				NDArray old_table = potential.table.exactClone();
				potential.table = NDArray.newArray(new_shape.clone());
				//System.out.println(Arrays.toString(potential.table.getShape()));
				//System.out.println(old_table.getShape());
				
				for (int i = 0; i < new_shape.length; i++) new_shape[i]--; //maxEntry[i] = old_shape[i] - 1;
				NDIndexIterator I = new NDIndexIterator(new_shape);
					
				do
				{
					int [] oo = new int [I.Iterator.length-1];
					for(int i=0;i<I.Iterator.length-1;i++){
						oo[i]=I.Iterator[i];
					}
					//System.out.println("$$$$$" + old_table.get(oo));
					potential.table.set(I.Iterator, old_table.get(oo));
				} while (I.increment());
				potential.nodes.add(n.name);
				//System.out.println(potential.nodes.toString()+"++++");
				//System.out.println(potential.table.toString());
			}


		}

		ghanta = potential.Clone();
	}
	public void reInitialize(){
		potential= ghanta.Clone();
		//ghanta tera backup
	}
	
	void InitPotential(Potential[] pots)
	{
		/**********************************/
	}
	
	@Override
	public String toString()
	{
		return Contents.toString();
	}
}
