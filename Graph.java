import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map.Entry;

public class Graph {
	List<Node> nodes;
	//List<List<Node>> adjacencyList;
	int numNodes;
	int numEdges;
	Map<Node, HashSet<Node>> adjList;
	int NumExtraEdges;
	static Potential[] potentials;
	
	Graph(int numnodes, List<Node> nodes)
	{
		this.numNodes = numnodes;
		this.nodes = nodes;
		this.numEdges = 0;
		adjList = new HashMap<Node, HashSet<Node>>();
		Iterator<Node> it = nodes.iterator();
		while (it.hasNext())
		{
			Node n = it.next();
			HashSet<Node> set = new HashSet<Node>();
			adjList.put(n, set);
		}
	}
	Graph(Graph G)
	{
		//Cloner cloner=new Cloner();

		this.numEdges = G.numEdges;
		this.numNodes = G.numNodes;
		this.NumExtraEdges = G.NumExtraEdges;
		//this.adjList = cloner.deepClone(G.adjList);
		//this.nodes = new ArrayList<Node>(G.nodes.size());
		//for (Node n: adjList.keySet())
		//{
		//	nodes.set(n.name, n);
		//}
				
		
		Node n;
		HashSet<Node> set, set1;
		Node[] array = new Node[G.nodes.size()];
		numNodes = G.numNodes;
		numEdges = G.numEdges;
		adjList = new HashMap<Node, HashSet<Node>>();
		Iterator<Entry<Node, HashSet<Node>>> it = G.adjList.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<Node, HashSet<Node>> entry = (Map.Entry<Node, HashSet<Node>>) it.next();
			n = entry.getKey();
			set = entry.getValue();
			Object clone = set.clone();
			set1 = (HashSet<Node>) clone;
			adjList.put(n, set1);
			array[n.name] = n;
		}
		nodes = new ArrayList<Node>(Arrays.asList(array));
		
	}
	public void DeleteNode(Node n)
	{
		//nodes.remove(n.name);
		/* delete from maps */
		HashSet<Node> set = adjList.get(n);
		for (Node n1 : set)
		{
			adjList.get(n1).remove(n);
		}
		adjList.remove(n);
	}
		
	
	public void InsertEdge(int n1, int n2) throws Exception
	{
		Node nn1=null, nn2=null;
		for (Node node  : nodes) {
			if (node.name == n1) {
				nn1 = node;
			}
		}
		for (Node node  : nodes) {
			if (node.name == n1) {
				nn2 = node;
			}
		}
		if(nn1!=null && nn2!=null)
			InsertEdge(nn1,nn2);
		else
			throw new Exception ("Nodes not found");
	}
	public void InsertEdge(Node n1, Node n2)
	{
		//System.out.println("+edge("+n1.name+","+n2.name+")");
		if (!adjList.get(n1).contains(n2))
		{
			adjList.get(n1).add(n2);
			adjList.get(n2).add(n1);
			n1.degree++;
			n2.degree++;
			numEdges++;
		}
	}
	
	public Graph Triangulate()
	{
		Graph G = new Graph(this);
		int numExtraEdges = 0;
		Object[] array = nodes.toArray().clone();
		Node n;
		HashSet<Node> set;
		Iterator<Node> it;
		
		
		for (int i = 0; i < array.length; i++)
		{
			Arrays.sort(array, i, array.length);
			n = (Node) array[i];
			set = adjList.get(n);
		//	System.out.print("\n"+n.name+":");
			for(Node n1: set) //n1
			{
		//		System.out.print(n1.name+",");
				for (Node n2: set) {//n2
			//		System.out.println("{"+n1.name+"=="+n2.name+"}?");

					if (n1.name != n2.name)
					{
						
						if ( !(G.adjList.get(n1).contains(n2)) ) 
							//add edges between all neighbors n1 and n2 of n
						{
							numExtraEdges++;
							//G.InsertEdge(G.nodes.get(n1.name), G.nodes.get(n2.name));
							G.InsertEdge(n1, n2);

							InsertEdge(n1, n2); /* include nodes in original graph*/
						}
					}
				}
			}
			DeleteNode(n);
		}
		G.NumExtraEdges = numExtraEdges;
		return G;
	}
	public boolean clique(HashSet<Node> vertices){
		boolean truth = true;
		Iterator<Node> itr= vertices.iterator();
		while(itr.hasNext()){
			Node n = itr.next();
			HashSet<Node> intersection = new HashSet<Node>(vertices);
			intersection.removeAll(adjList.get(n));
			if(!(intersection.size() == 1 && intersection.contains(n))){
				return false;
			}
		}
		return true;
	}	
		
	public List< HashSet < Node > > MaxCliques() throws Exception{
		HashSet<Node> L;
		List <Node> U = new LinkedList<Node> (nodes);
		L = new HashSet<Node>();
		List <Node> nodeOrdering = new ArrayList<Node>();
		List < HashSet < Node> > nbrInL = new ArrayList < HashSet <Node>>();
		List < HashSet < Node> > cliqueList = new ArrayList < HashSet <Node>>();
		for( int i =0;i< numNodes; i++){
			Iterator< Node> itr = U.iterator();
			int max = 0;
			Node newNode = U.get(0); 
			while(itr.hasNext()){
				Node n1 = (Node) itr.next();
				HashSet<Node> intersection = new HashSet<Node>(adjList.get(n1));
				intersection.retainAll(L);
				if(max<intersection.size()){
					max = intersection.size();
					newNode = n1;
				}
			}
			HashSet<Node> intersection = new HashSet<Node>(adjList.get(newNode));
			intersection.retainAll(L);
			if(!clique(intersection))
			{
				throw new Exception("Neighbours of "+newNode.name+" in L = "+L.toString()+" do not form a clique. The graph is not triangulated");
			}
			L.add(newNode);
			U.remove(newNode);
			nodeOrdering.add(newNode);
			nbrInL.add(intersection);
			//System.out.println(newNode.name+" "+intersection.size());
		}
		for(int i = 0 ;i <nbrInL.size()-1 ; i++){
			if(nbrInL.get(i+1).size()<nbrInL.get(i).size()+1){
				nbrInL.get(i).add(nodeOrdering.get(i));
				cliqueList.add(nbrInL.get(i));
			}
		}
		nbrInL.get(nbrInL.size()-1).add(nodeOrdering.get(nbrInL.size()-1));
		cliqueList.add(nbrInL.get(nbrInL.size()-1));

		return cliqueList;
	}
	public JTreeRooted createJT (List< HashSet < Node > > cliques){
		for(HashSet<Node> c : cliques){
			//System.out.println(c.toString());
		}
		List<JTNode> JTNodes = new ArrayList<JTNode> ();
		JTNodes.add(new JTNode (new ArrayList(cliques.get(0))));
		/*for(HashSet<Node> c : cliques){
			JTNodes.add(new JTNode(new ArrayList(c)));
		}*/
		List<HashSet<Node>> Done = new ArrayList<HashSet<Node>> ();
		Done.add(cliques.get(0));

		List<HashSet<Node>> Remaining = new LinkedList<HashSet<Node>> (cliques);
		Remaining.remove(cliques.get(0));
		JTNode root = JTNodes.get(0);
		Map<JTNode, JTNode> Parent = new HashMap <JTNode,JTNode> () ;
		Parent.put(root, null);
		Map<JTNode, ArrayList<JTNode>> Children = new HashMap<JTNode, ArrayList<JTNode>>();
		Children.put(root,new ArrayList<JTNode> ());


		while(Remaining.size()!=0){
			HashSet<Node> inner = null; 
			HashSet<Node> outer = null; 
			int innerIndex = 0;
			HashSet<Node> outerWinner = Remaining.get(0); 
			List<Node> inter = new ArrayList<Node>();
			int max =0;
			for(int i=0;i<Done.size();i++){
				inner = Done.get(i);
				for(int j=0;j<Remaining.size();j++){
					outer = Remaining.get(j);
					HashSet<Node> temp = new HashSet<Node> (Remaining.get(j));
					temp.retainAll(Done.get(i));
					if(max<temp.size()){
						max = temp.size();
						innerIndex = i;
						outerWinner = outer;
						inter = new ArrayList<Node>(temp);
					}
				}
			}
			JTNode outerNode = new JTNode(new ArrayList<Node>(outerWinner));
			JTNode innerNode = JTNodes.get(innerIndex);
			Parent.put(outerNode, innerNode);
			ArrayList<JTNode> innerNodeChildren = null;
			if(Children.containsKey(innerNode)){
 				innerNodeChildren = Children.get(innerNode);
			}
			else {
				innerNodeChildren = new ArrayList<JTNode> ();
			}
			innerNodeChildren.add(outerNode);
			Children.put(innerNode, innerNodeChildren);
			Children.put(outerNode, new ArrayList<JTNode> ());
			JTNodes.add(outerNode);
			new JTEdge(JTNodes.get(innerIndex),outerNode,new ArrayList(inter));
			Done.add(outerWinner);
			
			Remaining.remove(outerWinner);
		}

		List< HashSet<Integer>> faltu = new ArrayList<HashSet<Integer>>();

		for(HashSet<Node> set : Done){
			HashSet<Integer> faltuChild = new HashSet<Integer>();
			for(Node n : set){
				faltuChild.add(n.name);
			}
			faltu.add(faltuChild);
		}
		for(int i=0;i<potentials.length;i++){
			//System.err.println(potentials[i].table.toString());
			HashSet<Integer> potSet = new HashSet(potentials[i].nodes);
			boolean fap = false;
			for(int j=0;j<faltu.size();j++){
				if(faltu.get(j).containsAll(potSet)){
					JTNodes.get(j).multPotential(potentials[i]);
					fap = true;
					break;
				}
			}
			if(!fap){
				//System.err.println("all work no play");
			}
		}
		for(JTNode jtn : JTNodes){
			if(jtn.Contents.size() > jtn.potential.nodes.size()){
				jtn.dummyFill();
			}
			//System.err.println(jtn.Contents.toString()+" > " +Arrays.toString(jtn.potential.table.getShape())+ " > "+jtn.potential.table.toString());
		}
		JTreeRooted jt = new JTreeRooted(root, Parent, Children,this);
		return jt;
	}

	
	public static Graph ReadGraph(String graphfile, String potfile) throws IOException
	{
		int n, m;
		
		BufferedReader br = new BufferedReader(new FileReader(graphfile));
		String line;
		String[] tokens;
		line = br.readLine();
		n = Integer.parseInt(line);
		line = br.readLine();
		m = Integer.parseInt(line);
		List<Node> nodelist = new ArrayList<Node>(n);
		for (int i = 0; i < n ; i++)
		{
			line = br.readLine();
			tokens = line.split(" ");
			int id = Integer.parseInt(tokens[0]);
			int x = Integer.parseInt(tokens[1]);
			Node N = new Node(id, x);
			nodelist.add(id, N);
		}
		Graph G = new Graph(n, nodelist);
		for (int i = 0; i < m; i++)
		{
			line = br.readLine();
			tokens = line.split(" ");
			int n1 = Integer.parseInt(tokens[0]);
			int n2 = Integer.parseInt(tokens[1]);
			Node N1 = nodelist.get(n1), N2 = nodelist.get(n2);
			G.InsertEdge(N1, N2);
		}
		br.close();
		Potential temp = new Potential();
		potentials =  temp.ReadPotentials(potfile, G);
		return G;
	}
	public void PrintGraph(){
		Iterator<Node> itr = nodes.iterator();
		while(itr.hasNext()){
			Node cur = itr.next();
			Iterator<Node> itr1 = adjList.get(cur).iterator();
			System.out.print(cur.name+": ");
			while(itr1.hasNext()){
				Node curAdj = itr1.next();
				System.out.print(curAdj.name+" ");
			}
			System.out.println();
		}
	}

}
