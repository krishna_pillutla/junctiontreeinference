import java.util.ArrayList;
import java.util.List;

import mikera.arrayz.NDArray;


public class JTEdge {
	JTNode x;
	JTNode y;
	ArrayList<Node> Separator;
	Potential potential;
	public JTEdge(JTNode x, JTNode y, ArrayList<Node> separator)
	{
		this.x = x;
		this.y = y;
		this.Separator = separator;
		ArrayList<Integer> list = new ArrayList<Integer>(Separator.size());
		int[] shape = new int[Separator.size()];
		int count = 0;
		for (Node n : Separator)
		{
			list.add(n.name);
			shape[count] = n.maxValue;
			count++;
		}
		
		NDArray table = NDArray.newArray(shape);
		table.fill(1); //initialize to 1;
		potential = new Potential(Separator, table, list);
		
		x.EdgeWith.put(y, this);
		y.EdgeWith.put(x, this);
	}
}
