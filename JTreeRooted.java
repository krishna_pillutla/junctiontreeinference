import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mikera.arrayz.NDArray;

//String filename = "/Users/krishnapillutla/Documents/sem 8/ML/hw3/TestCases/graph_1";

public class JTreeRooted{
	JTNode root;
	Map<JTNode,JTNode> Parent;
	Map<JTNode, ArrayList<JTNode>> Children;
	int op = 0; //max
	Graph g;
	int[] BestAssignment;
	public JTreeRooted(JTNode root, Map<JTNode, JTNode> Parent, Map<JTNode, ArrayList<JTNode>> Children, Graph g)
	{
		this.g = g;
		this.Parent = Parent;
		this.Children = Children;
		this.root = root;
	}
	public void printShit(){
		for(JTNode j : Parent.keySet()){
	//		System.err.println("*"+j.Contents.toString());
	//		System.err.println("*\t"+j.potential.nodes.toString());
		}
	}
	
	void Reinitialize()
	{
		Set<JTNode> set = Parent.keySet();
		for (JTNode x : set)
		{
			//System.out.println(x.potential.nodes.toString());
			x.reInitialize();
		}
	}
	
	double operator(double op1, double op2)
	{
		if (op == 0) return Math.max(op1, op2);
		else return (op1 + op2);
	}
	
	public int[] MAP()
	{
		BestAssignment = new int[g.nodes.size()];
		MessagePassing(0);
		return BestAssignment;
	}
	
	boolean containsAll(ArrayList<Integer> contents, int[] elems)
	{
		for (int i = 0; i < elems.length; i++)
		{
			if (!contents.contains(elems[i])) return false;
		}
		return true;
	}
	JTNode FindContainingClique(int[] query)
	{
		Set<JTNode> set = Parent.keySet();
		for (JTNode x : set)
		{
			if (containsAll(x.potential.nodes, query)) return x;
		}
		assert false : "All query variables do not lie in a single node";
		return null;
		
		
	}
	public Potential AnswerQuery(int[] query)
	{
		JTNode node = FindContainingClique(query);
		MessagePassing(1);
		Potential pot = node.potential.Clone();
		ArrayList<Integer> querY = new ArrayList<Integer>(query.length); 
		for (int val : query)
		{
			querY.add(val);
		}
		Potential pot1 = Marginalise(pot, (querY));
		Normalize(pot1);
		return pot1;
	}
	public Potential[] AnswerQuery(ArrayList<int[]> queries)
	{
		Potential[] pots = new Potential[queries.size()];
		int i = 0;
		for (int[] q : queries)
		{
			pots[i++] = AnswerQuery(q);
		}
		return pots;
	}
	static int[] StringToInt(String[] arg)
	{
		int[] arr = new int[arg.length];
		for (int i = 0; i < arg.length; i++)
		{
			arr[i] = Integer.parseInt(arg[i]);
		}
		return arr;
	}
	
	public static ArrayList<int[]> ReadQueryFile(String filename) throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line = br.readLine();
		String[] parts;
		int numQ = Integer.parseInt(line);
		ArrayList<int[]> lst = new ArrayList<int[]>(numQ);
		for (int i = 0; i < numQ; i++)
		{
			line = br.readLine();
			parts = line.split(" ");
			lst.add(StringToInt(parts));
		}
		br.close();
		return lst;
	}
	
	void Normalize(Potential p)
	{
		double val = p.table.elementSum();
		if (val != 0)
			p.table.divide(val);
	}
	void Normalize(Potential[] p)
	{
		for (Potential pot: p)
		{
			Normalize(pot);
		}
	}
	
	/*Implementation of the Hugin Algorithm */
	void Update(JTNode u, JTNode w)
	{
		JTEdge separator = u.EdgeWith.get(w);
		Potential old_w_pot = w.potential, old_edge_pot = separator.potential;
		Potential new_edge_pot = Marginalise(u.potential,// entire potential 
				old_edge_pot.nodes //set of separator nodes
				);
		Potential new_w_pot = UpdateEdgePotential(old_w_pot, old_edge_pot, new_edge_pot);
		Normalize(new_w_pot);	
		//Updates
		separator.potential = new_edge_pot;
		w.updatePotential(new_w_pot);
	}
	
	/**
	 * @param original_pot f(u, v): 
	 * @param separator_nodes: v : Nodes that have be there is the final form (do not marginalise over these)
	 * @return g(v) = sum_u f(u,v)
	 */
	Potential Marginalise(Potential original_pot, ArrayList<Integer> separator_nodes)
	{
		return Marginalise_helper(original_pot, separator_nodes, 0);
	}
	
	int[] getSubArray(int[] old_shape, int excluded_index)
	{
		int[] shape = new int[old_shape.length - 1];
		int j = 0;
		for (int i = 0; i < shape.length; i++)
		{
			if (j == excluded_index) j++;
			shape[i] = old_shape[j];
			j++;
		}
		return shape;
	}
	int[] dereference(ArrayList<Integer> l, int[] arr)
	{
		//clique_nodes.get(start_index);
		int[] x = new int[arr.length];
		for (int i = 0; i < arr.length; i++)
		{
			x[i] = l.get(arr[i]);
		}
		return x;
	}
	/**
	 * @param original_pot f(u, v): 
	 * @param separator_nodes: v : Nodes that have be there is the final form (do not marginalise over these)
	 * @param start_index(si): Index to the variable currently being eliminated. Eg, x0, x1.
	 * @return g(u, v \ x_si) = sum_(x_si) f(u,v), if x_si does not belong to v. Else, skip over x_si (that is, si++).
	 */
	Potential Marginalise_helper(Potential original_pot, ArrayList<Integer> separator_nodes, 
			int start_index)
	{
		/* start from the start_index^{th} variable.
		 * Marginalise it out recursively. Return an (n-1) dim potential
		 */
		ArrayList<Integer> clique_nodes = original_pot.nodes;
		if (original_pot.nodes.size() == separator_nodes.size())
		{
			return original_pot;
		}
		if (separator_nodes.contains(clique_nodes.get(start_index)))
		{  
			//no need to marginalise over this variable. Skip it.
			return Marginalise_helper(original_pot,separator_nodes, start_index + 1);
		}
		
		/* marginalise over the start_index^{th} dimension now */
		
		/* step 1: fill in potential's info about variables */
		ArrayList <Integer> new_list = new ArrayList<Integer>(clique_nodes.size() - 1);
		ArrayList <Node> new_nlist = new ArrayList<Node>(clique_nodes.size() - 1);
		int start_var = clique_nodes.get(start_index);
		for (Integer x : clique_nodes)
		{
			if (x != start_var) new_list.add(x); //(n-1) dimensional
		}
		for (Node n: original_pot.clique)
		{
			if (n.name != start_var) new_nlist.add(n);
		}
		
		/* step 2: create empty table of the right size */
		int[] old_shape =  original_pot.table.getShape();
		int[] shape = getSubArray(old_shape, start_index);
		
		NDArray old_table = original_pot.table;
		NDArray table = NDArray.newArray(shape);
		table.fill(0);
		
		/*step 3: fill in the table */
		//one cell of the new table comes from an entire row of the old table
		int[] maxEntry = old_shape.clone();
		int[] new_iterator;
		for (int i = 0; i < maxEntry.length; i++) maxEntry[i]--; //maxEntry[i] = old_shape[i] - 1;
		NDIndexIterator I = new NDIndexIterator(maxEntry);
		//System.err.println("Eliminating over " + start_index + "th element: "  + start_var);
		//System.err.println("\t" + original_pot.nodes.toString());
		do
		{	
			new_iterator = getSubArray(I.Iterator, start_index);
			//System.err.println("\t eliminating " + Arrays.toString(I.Iterator) + "  " + Arrays.toString(new_iterator));
			double new_val = operator(table.get(new_iterator), old_table.get(I.Iterator));
			
			//can be either addition or maximum
			if (op == 0) //max
			{
				if (table.get(new_iterator) < old_table.get(I.Iterator))
				{
					//System.out.println("Original distribution " + Arrays.toString(I.Iterator) +  " " + old_table.get(I.Iterator));
					//System.out.println("new distribution " + Arrays.toString(new_iterator) +  " " + table.get(new_iterator));
					
					//update max
					BestAssignment[start_var] = I.Iterator[start_index];
				}
			}
			table.set(new_iterator, new_val); //increment by the current iterator's value
		} while (I.increment());
		
		Potential potpot =  new Potential(new_nlist, table, new_list);
		//System.out.println(new_list.toString()+">"+ potpot.table.toString());
		return Marginalise_helper(potpot, separator_nodes, start_index);
	}
	
	void MessagePassing(int op)
	{
		Reinitialize();
		this.op = op;
		CollectEvidence(root);
		DistributeEvidence(root);
	}
	
	void CollectEvidence(JTNode n)
	{
		//System.err.println("Called Collect Evidence with " + n.toString());
		for (JTNode child : Children.get(n))
		{
			CollectEvidence(child);
			Update(child, n);
		}
	}
	void DistributeEvidence(JTNode n)
	{
		for (JTNode child : Children.get(n))
		{
			Update(n, child);
			DistributeEvidence(child);
		}
	}
	
	static int[] SubArray(int[] array, ArrayList<Integer> indices)
	{
		int[] subarray = new int[indices.size()];
		for (int i = 0; i < subarray.length; i++)
		{
			subarray[i] = array[indices.get(i)];
		}
		return subarray;
	}
	
	Potential UpdateEdgePotential(Potential old_node_pot, Potential old_edge_pot, Potential new_edge_pot)
	{
		//new_node_pot = old_node_pot * new_edge_pot / old_edge_potential
		ArrayList<Integer> indices = new ArrayList<Integer> (old_edge_pot.nodes.size());
		//variables at these positions are also present in the edge potential
		int ii = 0;
		//System.err.println(old_node_pot.table.toString());
		//System.err.println("\t"+old_edge_pot.nodes.toString());
		for (Integer x : old_node_pot.nodes)
		{
			if (old_edge_pot.nodes.contains(x))
			{
				indices.add(ii);
			}
			ii++;
		}
		
		
		NDArray new_table = NDArray.newArray(old_node_pot.table.getShape());
		NDArray old_table = old_node_pot.table;
		new_table.fill(1);
		
		int[] maxEntry = old_node_pot.table.getShapeClone();
		for (int i = 0; i < maxEntry.length; i++) maxEntry[i]--; //maxEntry[i] = old_shape[i] - 1;
		NDIndexIterator I = new NDIndexIterator(maxEntry);

		do
		{

			int[] ind = SubArray(I.Iterator, indices);
			double val =   old_edge_pot.table.get(ind);
			//System.err.println(Arrays.toString(I.Iterator));
			//System.err.println("\t" + new_edge_pot.nodes.toString()); 
			double val1 = new_edge_pot.table.get(ind) ;
			//System.err.println(val1);
			if (val != 0) val= old_table.get(I.Iterator) * val1/ val;
			new_table.set(I.Iterator, val);
		} while (I.increment());
		
		return new Potential(old_node_pot.clique, new_table, new ArrayList<Integer>(old_node_pot.nodes));
		
	}
	
}
