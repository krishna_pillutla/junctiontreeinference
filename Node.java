
public class Node implements Comparable<Node>{
	String label;
	int name;
	int id;
	int maxValue;
	public int degree;
	
	public Node(int name, int maxval)
	{
		this.name = name;
		id = -1;
		maxValue = maxval;
		degree = 0;
	}
	public Node(Node n)
	{
		label = n.label;
		name = n.name;
		id = n.id;
		maxValue = n.maxValue;
		degree = n.degree;
	}
	/*
	 *a negative integer, zero, or a positive integer as this object is less than, 
	 *equal to, or greater than the specified object.
	 */
	@Override
	public int compareTo(Node n) {
		return (degree - n.degree); /* for ascending order*/
	}
	@Override 
	public String toString(){
		return new String(""+name);
	}
}
